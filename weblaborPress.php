<?php
/*
  Plugin Name: WeblaborPress
  Description: A tool for the press
  Author: Carlos Escobar
  Author URI: http://www.weblabor.mx
  Text Domain: weblaborpress
  Version: 0

  Licenced under the GNU GPL:

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* Add admin options page */
add_action('admin_menu', 'weblaborpress_admin_menu');
add_action( 'admin_enqueue_scripts', 'safely_add_stylesheet' );

add_filter('manage_users_columns', 'pippin_add_user_id_column');
function pippin_add_user_id_column($columns) {
    $columns['admin_control'] = 'Administration';
    return $columns;
}
 
add_action('manage_users_custom_column',  'pippin_show_user_id_column_content', 10, 3);
function pippin_show_user_id_column_content($value, $column_name, $user_id) {
	if( 'admin_control' != $column_name )
		return $value;
    return "<a href='admin.php?page=weblaborpress-finances&see=".$user_id."'>Ir a control</a>";
}

global $weblabor_db_version;
$weblabor_db_version = '1.0';

function weblabor_install() {
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

	global $wpdb;
	global $weblabor_db_version;

	$charset_collate = $wpdb->get_charset_collate();
	$table_name = $wpdb->prefix . 'weblaborpress_eventos';
	$sql = "CREATE TABLE $table_name (
		idEvento mediumint(9) NOT NULL AUTO_INCREMENT,
		fecha datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
		titulo varchar(55) NOT NULL,
		texto text NOT NULL,
		tipo varchar(55) NOT NULL,
		hecho int(1) NOT NULL,
		creador int(4),
		costoEvento int(4) NOT NULL,
		costoNota int(4) NOT NULL,
		idNota int(9),
		fechaCreacion datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
		estado int(3) DEFAULT 0,
		observaciones varchar(55),
		estadoObs int(1) DEFAULT 0,
		UNIQUE KEY id (idEvento)
	) $charset_collate;";
	dbDelta( $sql );

	$table_name = $wpdb->prefix . 'weblaborpress_opciones';
	$sql = "CREATE TABLE $table_name (
		idOpcion mediumint(9) NOT NULL AUTO_INCREMENT,
		propiedad varchar(55) NOT NULL,
		valor varchar(55) NOT NULL,
		UNIQUE KEY id (idOpcion)
	) $charset_collate;";
	dbDelta( $sql );

	$table_name = $wpdb->prefix . 'weblaborpress_asignada';
	$sql = "CREATE TABLE $table_name (
		idAsignada mediumint(9) NOT NULL AUTO_INCREMENT,
		tipoPersona int(4) NOT NULL,
		idEvento mediumint(9) NOT NULL,
		idUsuario int(9) NOT NULL,
		pagado int(1) DEFAULT 0,
		UNIQUE KEY id (idAsignada)
	) $charset_collate;";
	dbDelta( $sql );

	add_option( 'weblabor_db_version', $weblabor_db_version );
}

function weblabor_install_data() {
	global $wpdb;
	
	$table_name = $wpdb->prefix . 'weblaborpress_opciones';
	
	$wpdb->insert( 
		$table_name, 
		array( 
			'propiedad' => 'precio_nota', 
			'valor' => '12', 
		) 
	);

	$wpdb->insert( 
		$table_name, 
		array( 
			'propiedad' => 'precio_noche', 
			'valor' => '80', 
		) 
	);

	$wpdb->insert( 
		$table_name, 
		array( 
			'propiedad' => 'precio_evento', 
			'valor' => '2', 
		) 
	);
}

register_activation_hook( __FILE__, 'weblabor_install' );
register_activation_hook( __FILE__, 'weblabor_install_data' );

/**
 * Add stylesheet to the page
 */
function safely_add_stylesheet() {
    wp_enqueue_style( 'prefix-style', plugins_url('style.css', __FILE__) );
}

function weblaborpress_admin_menu() {
	global $wp_version;

	add_menu_page('Control de Prensa', 'Control de Prensa', 'edit_posts', 'weblaborpress', 'weblabor_eventos');
	add_submenu_page( 'weblaborpress', 'Agregar evento', 'Agregar evento', 'edit_posts', 'weblaborpress-add', 'weblabor_add' );
	add_submenu_page( 'weblaborpress', 'Control financiero', 'Control financiero', 'edit_posts', 'weblaborpress-finances', 'weblabor_control' );
	add_submenu_page( 'weblaborpress', 'Observaciones', 'Observaciones', 'edit_posts', 'weblaborpress-observations', 'weblabor_observaciones' );
	add_submenu_page( 'weblaborpress', 'Pagos', 'Pagos', 'manage_options', 'weblaborpress-pagos', 'weblabor_pagos' );
	add_submenu_page( 'weblaborpress', 'Administración', 'Administración', 'manage_options', 'weblaborpress-admin', 'weblabor_opciones' );
	return;
}

/* Vistas */
function weblabor_eventos()	{	
	global $wpdb;	
	global $current_user;
	get_currentuserinfo();
	if(isset($_GET["agregar"])) { // Si se está asignando a un evento
		$idEvento = $_GET['agregar'];
		$tipo = $_GET['tipo'];
		$id = $wpdb->get_var("SELECT idUsuario FROM {$wpdb->prefix}weblaborpress_asignada WHERE idEvento = $idEvento && tipoPersona=$tipo");
		if(isset($id)) {
			// Desasignar
			if($id==$current_user->ID) {
				$wpdb->delete("{$wpdb->prefix}weblaborpress_asignada", array("idEvento" =>$idEvento, "tipoPersona" => $tipo));
			} else {
				$wpdb->query(
					"
					UPDATE {$wpdb->prefix}weblaborpress_asignada
					SET idUsuario = $current_user->ID
					WHERE idEvento = $idEvento and tipoPersona = $tipo
					"
				);
			}
				
		} else {
			$wpdb->query(
				"
				INSERT INTO {$wpdb->prefix}weblaborpress_asignada
				(tipoPersona, idEvento, idUsuario)
				VALUES ($tipo, $idEvento, $current_user->ID)
				"
			);
		}
		echo "<div class='success'>Asignado correctamente</div>";

	}
	if(isset($_POST["id"])) { // Se marca algo como listo, paso 2, asignar
		$wpdb->query(
			"
			UPDATE {$wpdb->prefix}weblaborpress_eventos
			SET hecho = 1, idNota = ".$_POST["idPost"]."
			WHERE idEvento = ".$_POST["id"]."
			"
		);
		add_post_meta( $_POST["idPost"], 'weblabor_checked', $current_user->ID );
		echo "<div class='success'>Marcado como listo exitosamente</div>";
	}
	if(isset($_GET["eliminar"])) {  // Se elimina evento ?>
	<div class="wrap wbl">
		<h2>Cambiar estado</h2>
		<form action="admin.php?page=weblaborpress" method="POST">
			<input type="hidden" name="idACambiar" value="<?php echo $_GET["eliminar"]; ?>">
			<input type="hidden" name="estado" value="-1">
			<label>Observaciones</label><input type="text" name="observacion" required />
			<input type="submit" value="Enviar" />
		</form>
	</div>
	<?php  return;
	}
	if(isset($_GET["importante"])) { // se marca como importante
		$wpdb->query(
			"
			UPDATE {$wpdb->prefix}weblaborpress_eventos
			SET estado = 1
			WHERE idEvento = ".$_GET["importante"]."
			"
		);
		echo "<div class='success'>Marcado exitosamente</div>";  
	} 
	if(isset($_POST["estado"])) {
		// 0 = Normal, 1 = Importante, 2 = Sin importancia, -1 = Eliminado
		
		if($_POST["estado"]==2) {
			$wpdb->query(
				"
				UPDATE {$wpdb->prefix}weblaborpress_eventos
				SET costoNota = 0, estado = ".$_POST["estado"].", estadoObs = 1, observaciones = '".$_POST["observacion"]."'
				WHERE idEvento = ".$_POST["idACambiar"]."
				"
			);
		} else {
			$wpdb->query(
				"
				UPDATE {$wpdb->prefix}weblaborpress_eventos
				SET estado = ".$_POST["estado"].", estadoObs = 1, observaciones = '".$_POST["observacion"]."'
				WHERE idEvento = ".$_POST["idACambiar"]."
				"
			);
		}
		echo "<div class='success'>Marcado exitosamente</div>";
	}
	if(isset($_GET["nopublicar"])) {  ?>
	<div class="wrap wbl">
		<h2>Cambiar estado</h2>
		<form action="admin.php?page=weblaborpress" method="POST">
			<input type="hidden" name="idACambiar" value="<?php echo $_GET["nopublicar"]; ?>">
			<input type="hidden" name="estado" value="2">
			<label>Observaciones</label><input type="text" name="observacion" required />
			<input type="submit" value="Enviar" />
		</form>
	</div>
	<?php  return;
	}
	// Notificación
	$mensajes = $wpdb->get_var("SELECT count(*) FROM {$wpdb->prefix}weblaborpress_eventos WHERE creador = $current_user->ID and observaciones != '' and estadoObs = 1");
	if($mensajes > 0) {
		echo "<div class='success'><a href='admin.php?page=weblaborpress-observations'>Tienes observaciones por ver</a></div>";
	}
	if(isset($_GET["listo"])) {  // Se esta marcando como listo, paso 1, seleccionar nota ?>
	<div class="wrap wbl">
		<h2>Eventos listos</h2>
		<form action="admin.php?page=weblaborpress" method="POST">
			<input type="hidden" name="id" value="<?php echo $_GET["listo"]; ?>">
			<label>Titulo</label><?php echo $_GET["titulo"]; ?><br />
			<label>Post</label>
			<select name="idPost" required>
			<?php 
 			$args = array(
 				'post_type' => array("postales","post","increible"),
				'author_name'    => $current_user->user_nicename,
				'post_status' => 'publish',
				'meta_query' => array(
				   'relation' => 'OR',
				    array(
				     'key' => 'weblabor_checked',
				     'compare' => 'NOT EXISTS', // works!
				     'value' => '' // This is ignored, but is necessary...
				    ),
				    array(
				     'key' => 'weblabor_checked',
				     'compare' => '!=',
				     'value' => $current_user->ID
				    )
				)
			);

			query_posts($args);
			if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			
				<option value="<?php the_id(); ?>"><?php the_title(); ?></option>
			
			<?php endwhile; else:
			echo "Sin notas para seleccionar";
			endif;?>
			</select><br />
			<input type="submit" value="Enviar" />
		</form>
	</div>

	<?php return;
	}
	$CLEAN["tipo"] = -1;
	$CLEAN["estado"] = 6;
	if(isset($_POST["tipo"])) { // Se está filtrando
		$CLEAN["tipo"] = $_POST["tipo"];
	}
	if(isset($_POST["estado"])) { // Se está filtrando
		$CLEAN["estado"] = $_POST["estado"];
	}
	$select = "";
	$tipo = "";
	$having = "";
	$naturaljoin = "";
	if($CLEAN["tipo"]!=-1) {
		$tipo = "tipo = ".$CLEAN["tipo"];
	}
	$estado = "";
	if($CLEAN["estado"]==-1) {
		// No hacer nada
	} else if($CLEAN["estado"]==0) {
		// Normal pero sin haber pasado y no se ha subido
		$estado = "estado = 0 and idNota IS NULL";
		$select = "(DATEDIFF(fecha,Now())) as tiempo";
		$having = "fecha = '0000-00-00 00:00' || tiempo > 0";
	} else if($CLEAN["estado"]<=2) {
		// Estados existentes
		$estado = "estado = ".$CLEAN["estado"]." and idNota IS NULL";
	} else if($CLEAN["estado"]==3) {
		// Evento ya realizado
		$estado = "idNota IS NULL";
		$select = "(DATEDIFF(fecha,Now())) as tiempo";
		$having = "tiempo <= 0";
	} else if($CLEAN["estado"]==4) {
		// Tus eventos
		$naturaljoin = "{$wpdb->prefix}weblaborpress_asignada";
		$estado = "idUsuario = ".$current_user->ID." and idNota IS NULL";
	} else if($CLEAN["estado"]==5) {
		// Notas subidas
		$estado = "idNota IS NOT NULL";
	} else if($CLEAN["estado"]==6) {
		// Activas
		$estado = "hecho = 0 and estado != 2 and idNota IS NULL";
		$select = "(DATEDIFF(fecha,Now())) as tiempo";
		$having = "fecha = '0000-00-00 00:00' || tiempo > 0";
	}
	$where = "";
	if(strlen($estado)>0 && strlen($tipo)>0) {
		$where = "WHERE $estado and $tipo and estado != -1";
	} else if(strlen($estado)>0 || strlen($tipo)>0) {
		$where = "WHERE $estado $tipo and estado != -1";
	}
	if(strlen($select)==0) {
		$select = "*";
	} else {
		$select = "*, $select";
	}
	if(strlen($having)>0) {
		$having = "HAVING $having";
	} 
	if(strlen($naturaljoin)>0) {
		$naturaljoin = "NATURAL JOIN $naturaljoin";
	}
	$select = 
		"
		SELECT $select
		FROM {$wpdb->prefix}weblaborpress_eventos
		$naturaljoin
		$where
		$having
		ORDER BY fecha ASC
		LIMIT 50
		";
	$eventos =  $wpdb->get_results($select);
		
	?>

	<div class="wrap wbl">
		<h2>Eventos</h2>
		<b>Notas creadas: </b><?php echo countNotas($current_user->ID); ?> | <b>Galerías creadas: </b><?php echo countFotos($current_user->ID); ?> | <b>Eventos creados: </b><?php echo countEvents($current_user->ID); ?> | <b>Hora servidor: </b><?php echo date("Y-m-d h:i:s"); ?><br />
	  	<form action="admin.php?page=weblaborpress" method="POST">
	  		<select name="tipo">
		  		<option value="-1">Todos los tipos</option>
		  		<option value="0">Evento</option>
	  			<option value="1">Nota previa</option>
	  			<option value="2">Entrevista</option>
	  			<option value="3">Rueda de prensa</option>
	  			<option value="4">Sesión de fotos</option>
	  			<option value="5">Sección especial</option>
		  	</select>
		  	<select name="estado">
		  		<option value="6">Activas</option>
		  		<option value="-1">Todos los estados</option>
		  		<option value="3">Fecha expirada</option>
		  		<option value="4">Asignado a ti</option>
		  		<option value="5">Publicado</option>
		  		<option value="1">Importante</option>
		  		<option value="2">No publicar</option>
		  		<option value="0">Estado normal</option>
		  	</select>
		  	<input type="submit" class="boton" value="Filtrar" />
		  </form>
	  	<br />

	  	<span class="cuadro" style="background: #ee9292;"></span>Fecha expirada <span class="cuadro" style="background: #f0e8db;"></span>Asignado a ti <span class="cuadro" style="background: #cde3c3;"></span>Publicado <span class="cuadro" style="background: #f0eab8;"></span>Importante <span class="cuadro" style="background: #f9f9f9;"></span>No publicar <br /><br />
	  	<table style="width:100%">
	  		<tr class="head">
	  			<th>Titulo</th>
	  			<th>Tipo</th>
	  			<th>Asignado a</th>
	  			<th>Fecha</th>
	  			<th>Horario</th>
	  			<th>Apoyo</th>
	  			<th>Más información</th>
	  		</tr>
	  		<?php 
	  		foreach ( $eventos as $evento ) 
			{
				$bool = true;
				$bool2 = true;
				echo "<tr";
				if($evento->hecho==1) {
					echo " class='listo' ";
					$bool = false;
				} else if(participaEnEvento($evento->idEvento)) {
					echo " class='participa' ";
					$bool2 = false;
				} else if(fechaNoActiva($evento->fecha)) {
					echo " class='noactiva' ";
					$bool = false;
				} else if($evento->estado==1) {
					echo " class='importante' ";
				} else if($evento->estado==2) {
					echo " class='noimportante' ";
				}
				if($evento->tipo==2 && eventoEstaAsignado($evento->idEvento)) {
					$bool = false;
				}		
				echo ">";
				echo "<td class='name'> $evento->titulo <div class='info2'> ".setFormatToText($evento->texto)."</div></td>";
				echo "<td> ".getTypeEvent($evento->tipo)." </td>";
				echo "<td> ".eventoAsignadoA($evento, $bool,$bool2)." </td>";
				echo "<td> $evento->fecha </td>";
				echo "<td> ".getTimeOfDayEvent($evento->fecha)." </td>";
				echo "<td> $".$evento->costoNota." </td>";
				echo "<td>";
				if((is_super_admin() || (esCreadorEvento($evento->idEvento)) && $evento->hecho==0)) {
					echo "<a href='admin.php?page=weblaborpress-add&edit=$evento->idEvento'>Editar</a> ";	
					echo "<a href='admin.php?page=weblaborpress&eliminar=$evento->idEvento'>Eliminar</a> ";
				}
				if(participaEnEvento($evento->idEvento) && $evento->hecho==0) {
					echo "<a href='admin.php?page=weblaborpress&listo=$evento->idEvento&titulo=$evento->titulo'>Listo</a> ";
				}
				if(is_super_admin() && $evento->hecho==0) {
					echo "<a href='admin.php?page=weblaborpress&importante=$evento->idEvento'>Importante</a> ";
					echo "<a href='admin.php?page=weblaborpress&nopublicar=$evento->idEvento'>No publicar</a> ";
				}

				echo "<a class='name' href='#'>Más info<div class='info2 infolarge inforight'>Creado por: ".getUserName($evento->creador)."<br />El día: ".$evento->fechaCreacion."</div></a>";
				echo "</td>";
				echo "</tr>";
			}
	  		?>
		</table>

	<?php		
}	
function setFormatToText($text) {
	$text = nl2br($text);
	//filtro los enlaces normales
	$text = preg_replace("/((http|https|www)[^\s]+)/", '<a href="$1" target="_blank">$0</a>', $text);
	//miro si hay enlaces con solamente www, si es así le añado el http://
	$text = preg_replace("/href=\"www/", 'href="http://www', $text);
	return $text;
}
function weblabor_add()	{	
	global $wpdb;	
	global $current_user;
	get_currentuserinfo();
	$edit = 0;
	$default = array("",0,"","","");
	if(isset($_POST["titulo"])) { // Agregar evento
		$precio = $wpdb->get_var("SELECT valor FROM {$wpdb->prefix}weblaborpress_opciones WHERE propiedad = 'precio_nota'");
		$fecha = $_POST['fecha']." ".$_POST['hora'];
		if(getTimeOfDayEvent($fecha)=="Nocturno" && $_POST['tipo']!=1) {
			$precio = $wpdb->get_var("SELECT valor FROM {$wpdb->prefix}weblaborpress_opciones WHERE propiedad = 'precio_noche'");				
		}
		if($_POST["id"]==0) {
			$precio_evento = $wpdb->get_var("SELECT valor FROM {$wpdb->prefix}weblaborpress_opciones WHERE propiedad = 'precio_evento'");
			$wpdb->query(
				"
				INSERT INTO {$wpdb->prefix}weblaborpress_eventos
				(fecha, titulo, texto, tipo, hecho, costoEvento, creador, fechaCreacion, costoNota)
				VALUES ('".$fecha."', '".$_POST['titulo']."', '".$_POST['texto']."', ".$_POST['tipo'].", 0, $precio_evento, $current_user->ID, '".date("Y-m-d h:i:s")."', $precio)
				"
			);
			echo "<div class='success'>Agregado correctamente</div>";
		} else {
			$wpdb->query(
				"
				UPDATE {$wpdb->prefix}weblaborpress_eventos
				SET fecha = '".$fecha."', titulo = '".$_POST['titulo']."', texto = '".$_POST['texto']."', tipo = ".$_POST['tipo'].", costoNota = $precio
				WHERE idEvento = ".$_POST['id']."
				"
			);
			if(isset($_POST["precio"])) {
				$wpdb->query(
					"
					UPDATE {$wpdb->prefix}weblaborpress_eventos
					SET costoNota = ".$_POST['precio'].", estado = ".$_POST['estado'].", idNota = ".$_POST['idNota'].", hecho = ".$_POST['hecho']."
					WHERE idEvento = ".$_POST['id']."
					"
				);
			}
			echo "<div class='success'>Actualizado correctamente</div>";
		}
		
	} elseif (isset($_GET["edit"])) {
		$edit = $_GET["edit"];
		$eventos =  $wpdb->get_results( 
			"
			SELECT *
			FROM {$wpdb->prefix}weblaborpress_eventos
			WHERE idEvento = ".$_GET['edit']."
			LIMIT 1
			"
		);
		foreach ( $eventos as $evento ) 
		{
			$default[0] = $evento->titulo;
			$default[1] = $evento->tipo;
			$fecha = explode(" ", $evento->fecha);
			$default[2] = $fecha[0];
			$default[3] = $fecha[1];
			$default[4] = $evento->texto;
			$default[5] = $evento->costoNota;
			$default[6] = $evento->estado;
			$default[7] = "NULL";
			if(strlen($evento->idNota)>0) {
				$default[7] = $evento->idNota;
			}
			$default[8] = $evento->hecho;
		}
	}
	?>
	<div class="wrap wbl">
		<h2>Agregar evento</h2><br />
		<b>Herramientas:</b> <a href="http://imgur.com/" target="_blank">Subir imagenes</a><br /><br />
	  	<form action="admin.php?page=weblaborpress-add" method="POST">
	  		<input type="hidden" name="id" value="<?php echo $edit; ?>">
	  		<label>Titulo *</label>
	  		<input name="titulo" type="text" value="<?php echo $default[0] ?>" required /><br/>
	  		<label>Tipo *</label>
	  		<select name="tipo" required />
	  			<option value="0"<?php if($default[1]==0) { echo " selected"; } ?>>Evento</option>
	  			<option value="1"<?php if($default[1]==1) { echo " selected"; } ?>>Nota previa</option>
	  			<option value="2"<?php if($default[1]==2) { echo " selected"; } ?>>Entrevista</option>
	  			<option value="3"<?php if($default[1]==3) { echo " selected"; } ?>>Rueda de prensa</option>
	  			<option value="4"<?php if($default[1]==4) { echo " selected"; } ?>>Sesión de fotos</option>
	  			<option value="5"<?php if($default[1]==5) { echo " selected"; } ?>>Sección especial</option>
	  		</select>
	  		<br/>
	  		<label>Fecha</label>
	  		<input name="fecha" type="date" placeholder="dd/mm/aaaa" style="width:150px;" value="<?php echo $default[2] ?>"><input name="hora" type="time" placeholder="hh:mm" value="<?php echo $default[3] ?>" style="width:150px;" /><br/>
	  		<?php if (is_super_admin() && isset($_GET["edit"])) { ?>
		  		<label>Precio</label>
	  			<input name="precio" type="text" value="<?php echo $default[5] ?>" /><br/>
	  			<label>Estado</label>
	  			<input name="estado" type="text" value="<?php echo $default[6] ?>" /><br/>
	  			<label>Nota</label>
	  			<input name="idNota" type="text" value="<?php echo $default[7] ?>" /><br/>
	  			<label>Hecho</label>
	  			<input name="hecho" type="text" value="<?php echo $default[8] ?>" /><br/>
		  	<?php } ?>
	  		<label>Más información</label><br />
	  		<textarea name="texto"><?php echo $default[4] ?></textarea><br/>
	  		<input type="submit" value="Agregar">
	  	</form>
	<?php		
}	
function weblabor_control()	{
	global $wpdb;
	global $current_user;
	get_currentuserinfo();
	$usuario = $current_user;
	if(isset($_GET["see"])) {
		$usuario = get_userdata($_GET["see"]);
	}
	if(isset($_GET["liberar"])) {
		$wpdb->query(
			"
			UPDATE {$wpdb->prefix}weblaborpress_eventos
			natural join {$wpdb->prefix}weblaborpress_asignada
			SET pagado = 1
			WHERE idUsuario = $usuario->ID
			"
		);
		echo "<div class='success'>Pago Liberado automaticamente</div>";
	}
	if(isset($_GET["limpiar"])) {
		$args = array(
			'author_name'    => $usuario->user_nicename,
			'post_status' => 'publish',
			'meta_query' => array(
			   'relation' => 'OR',
			    array(
			     'key' => 'weblabor_checked',
			     'compare' => 'NOT EXISTS', // works!
			     'value' => '' // This is ignored, but is necessary...
			    ),
			    array(
			     'key' => 'weblabor_checked',
			     'compare' => '!=',
			     'value' => $usuario->ID
			    )
			)
		);
		query_posts($args);
		$cont = 0;
		if ( have_posts() ) : while ( have_posts() ) : the_post(); 
			add_post_meta( get_the_id(), 'weblabor_checked', $usuario->ID );
			$cont++;
		endwhile; 
		else:
		endif;
		echo "<div class='success'>Limpiado el registro de notas (".$cont." notas)</div>";
	}
	$pendiente = $wpdb->get_var("
		SELECT SUM(costoNota)
		FROM {$wpdb->prefix}weblaborpress_eventos
		natural join {$wpdb->prefix}weblaborpress_asignada 
		WHERE hecho = 1 and idUsuario = $usuario->ID and pagado = 0
		LIMIT 50
		");
	if(strlen($pendiente)==0) {
		$pendiente = 0;
	}
	$pagado = $wpdb->get_var("
		SELECT SUM(costoNota)
		FROM {$wpdb->prefix}weblaborpress_eventos
		natural join {$wpdb->prefix}weblaborpress_asignada 
		WHERE hecho = 1 and idUsuario = $usuario->ID and pagado = 1
		LIMIT 50
		");
	if(strlen($pagado)==0) {
		$pagado = 0;
	}
	$notas =  $wpdb->get_results( 
		"
		SELECT post_title, guid, costoNota, pagado, idAsignada, idEvento, titulo
		FROM {$wpdb->prefix}weblaborpress_eventos
		natural join {$wpdb->prefix}weblaborpress_asignada 
		inner join $wpdb->posts on {$wpdb->posts}.ID = {$wpdb->prefix}weblaborpress_eventos.idNota
		WHERE hecho = 1 and idUsuario = $usuario->ID and pagado = 0
		LIMIT 50
		"
	);
	?>
	<div class="wrap wbl">
		<h2>Control financiero</h2>
		<h3><?php echo $usuario->display_name; ?></h3>
	  	<b>Total recibido:  </b>$<?php echo $pagado; ?><br />
	  	<b>Pendiente:  </b>$<?php echo $pendiente; ?><br /><br />
	  	<b>Notas:</b><br /><br />
	  	<table>
	  		<?php foreach ($notas as $nota) { ?>
	  		<tr>
	  			<td><a href="<?php echo $nota->guid ?>"><?php echo $nota->post_title ?></a></td>
	  			<td>$<?php echo $nota->costoNota ?></td>
	  			<td><?php echo $nota->titulo; ?></td>
	  			<td><?php echo estadoPago($nota->pagado); ?></td>
	  			<?php if (is_super_admin()) { ?>
	  				<td><a href="admin.php?page=weblaborpress-add&edit=<?php echo $nota->idEvento; ?>">Editar</a></td>
	  			<?php } ?>
	  		</tr>
	  		<?php } ?>
	  	</table>
	  	<?php if (is_super_admin()) { ?>
	  		<form action="admin.php?page=weblaborpress-finances&liberar=true&see=<?php echo $usuario->ID; ?>" method="POST">
	  			<input type="submit" value="Liberar el pago" />
	  		</form>
	  		<form action="admin.php?page=weblaborpress-finances&limpiar=true&see=<?php echo $usuario->ID; ?>" method="POST">
	  			<input type="submit" value="Limpiar notas" />
	  		</form>
	  	<?php } ?>
	<?php		
}	
function weblabor_observaciones()	{
	global $wpdb;
	global $current_user;
	get_currentuserinfo();
	$usuario = $current_user;
	if(isset($_GET["see"])) {
		$usuario = get_userdata($_GET["see"]);
	}
	$wpdb->query(
		"
		UPDATE {$wpdb->prefix}weblaborpress_eventos
		SET estadoObs = 0
		WHERE observaciones != '' and creador = $usuario->ID and estadoObs = 1
		"
	);
	$observaciones =  $wpdb->get_results( 
		"
		SELECT titulo, fechaCreacion, observaciones, estado
		FROM {$wpdb->prefix}weblaborpress_eventos
		WHERE observaciones != '' and creador = $usuario->ID
		LIMIT 50
		"
	);
	?>
	<div class="wrap wbl">
		<h2>Observaciones</h2>
		<h3><?php echo $usuario->display_name; ?></h3>
	  	<table>
	  		<?php foreach ($observaciones as $observacion) { ?>
	  		<tr>
	  			<td><?php echo getEstado($observacion->estado) ?></td>
	  			<td><?php echo $observacion->titulo ?></td>
	  			<td><?php echo $observacion->fechaCreacion ?></td>
	  			<td><?php echo $observacion->observaciones; ?></td>
	  		</tr>
	  		<?php } ?>
	  	</table>
	<?php		
}
function weblabor_pagos()	{
	global $wpdb;
	$pagos =  $wpdb->get_results("
		SELECT SUM(costoNota) as suma, user_login, idUsuario
		FROM {$wpdb->prefix}weblaborpress_eventos
		natural join {$wpdb->prefix}weblaborpress_asignada 
		inner join {$wpdb->prefix}users on idUsuario = {$wpdb->prefix}users.ID
		WHERE hecho = 1 and pagado = 0
		GROUP BY idUsuario
		HAVING suma > 0
		ORDER BY suma DESC
		LIMIT 50
		");
	?>
	<div class="wrap wbl">
		<h2>Pagos a realizar</h2>
	  	<table>
	  		<?php foreach ($pagos as $pago) { ?>
	  		<tr>
	  			<td><?php echo $pago->user_login; ?></td>
	  			<td><?php echo $pago->suma; ?></td>
	  			<td><a href="admin.php?page=weblaborpress-finances&see=<?php echo $pago->idUsuario ?>">Ir a control</td>
	  		</tr>
	  		<?php } ?>
	  	</table>
	<?php		
}
function weblabor_opciones()	{
	global $wpdb;	
	if(isset($_POST["precio_nota"])) {
		$wpdb->query(
			"
			UPDATE {$wpdb->prefix}weblaborpress_opciones
			SET valor = ".$_POST['precio_nota']."
			WHERE propiedad = 'precio_nota'
			"
		);
		$wpdb->query(
			"
			UPDATE {$wpdb->prefix}weblaborpress_opciones
			SET valor = ".$_POST['precio_noche']."
			WHERE propiedad = 'precio_noche'
			"
		);
		$wpdb->query(
			"
			UPDATE {$wpdb->prefix}weblaborpress_opciones
			SET valor = ".$_POST['precio_evento']."
			WHERE propiedad = 'precio_evento'
			"
		);
	}
	$precio_nota = $wpdb->get_var("SELECT valor FROM {$wpdb->prefix}weblaborpress_opciones WHERE propiedad = 'precio_nota'");
	$precio_noche = $wpdb->get_var("SELECT valor FROM {$wpdb->prefix}weblaborpress_opciones WHERE propiedad = 'precio_noche'");
	$precio_evento = $wpdb->get_var("SELECT valor FROM {$wpdb->prefix}weblaborpress_opciones WHERE propiedad = 'precio_evento'");
	?>
	<div class="wrap wbl">
		<h2>Opciones</h2><br />
	  	<form action="admin.php?page=weblaborpress-admin" method="POST">
	  		<label>Precio Nota</label>
	  		<input type="text" name="precio_nota" value="<?php echo $precio_nota ?>"><br/>
	  		<label>Precio Noche</label>
	  		<input type="text" name="precio_noche" value="<?php echo $precio_noche ?>"><br/>
	  		<label>Precio Evento</label>
	  		<input type="text" name="precio_evento" value="<?php echo $precio_evento ?>"><br/>
	  		<input type="submit" value="Actualizar">
	  	</form>
	<?php		
}	
/* Funciones */
function getEstado($id) {
	switch ($id) {
		case 2:
			return "Baja prioridad";
			break;
		case -1:
			return "Eliminado";
			break;
	}
}
function getTypeEvent($id) {
	switch ($id) {
		case 0:
			return "Evento";
			break;
		case 1:
			return "Nota previa";
			break;
		case 2:
			return "Entrevista";
			break;
		case 3:
			return "Rueda de prensa";
			break;
		case 4:
			return "Sesión de fotos";
			break;
		case 5:
			return "Sección especial";
			break;
	}
}
function getTimeOfDayEvent($hora) {
	$res = "";
	if(strlen($hora)>1) {
		$hora = explode(" ", $hora);
		$hora = explode(":", $hora[1]);
		$hora = $hora[0];
		if($hora>=20) {
			$res = "Nocturno";
		}else if($hora>=12) {
			$res = "Vespertino";
		} else {
			$res = "Matutino";
		}
	}
	return $res;
}	
function participaEnEvento($idEvento) {
	global $wpdb;
	global $current_user;
	get_currentuserinfo();
	$id = $wpdb->get_var("SELECT idUsuario FROM {$wpdb->prefix}weblaborpress_asignada WHERE idEvento = $idEvento");
	if(isset($id) && $id==$current_user->ID) {
		return true;
	}
	return false;
}
function esCreadorEvento($idEvento) {
	global $wpdb;
	global $current_user;
	get_currentuserinfo();
	$id = $wpdb->get_var("SELECT creador FROM {$wpdb->prefix}weblaborpress_eventos WHERE idEvento = $idEvento");
	if(isset($id) && $id==$current_user->ID) {
		return true;
	}
	return false;
}
function eventoEstaAsignado($idEvento) {
	global $wpdb;
	$id = $wpdb->get_var("SELECT idUsuario FROM {$wpdb->prefix}weblaborpress_asignada WHERE idEvento = $idEvento");
	if(strlen($id)>0) {
		return true;
	}
	return false;
}
function getUserName($id) {
	$user = get_userdata($id);
	return $user->display_name;
}
function eventoAsignadoA($evento, $bool, $b2) {
	/* 
		1 - Reportero
		2 - Fotografo
	*/
	global $wpdb;
	global $current_user;
	$idEvento = $evento->idEvento;
	$tipo = $evento->tipo;
	get_currentuserinfo();
	$id = $wpdb->get_var("SELECT idUsuario FROM {$wpdb->prefix}weblaborpress_asignada WHERE idEvento = $idEvento && tipoPersona=1");
	$bool2 = true;
	$html = "";
	$html .= "<b>Reportero:</b> ";
	if(isset($id)) {
		$user = get_userdata($id);
		$html .="<span class='name'>";
		if (is_super_admin()) { 
			$html .= "<a href='admin.php?page=weblaborpress-finances&see=$id'>";
		}
		$html .= $user->display_name;
		if (is_super_admin()) { 
			$html .= "</a>";
		}
		$html .="<div class='info'><b>Notas: </b>";
		$html .= countNotas($id);
		$html .="</div>";
		$html .="</span>";
		if(countNotas($id) > countNotas($current_user->ID) && $id != $current_user->ID) {
			$bool2 = false;
		}
		if($bool2 && $evento->fecha!="0000-00-00 00:00:00") {
			$fechaActual = new DateTime(date("Y-m-d H:m")); 
			$fecha = new DateTime($evento->fecha);
			$interval = date_diff($fechaActual, $fecha);
			if($interval->invert==0 && $interval->d<=5) {
				$bool2 = false;
			}
		}	
	}
	
	if(($bool && $bool2) || $evento->tipo==1) {
		$html .= " (<a href='admin.php?page=weblaborpress&agregar=$idEvento&tipo=1'>";
		if($id != $current_user->ID && $b2) {
			$html .= "Asignarme";
		} else if($id == $current_user->ID) {
			$html .= "Desasignarme";
		}
		$html .= "</a>)";		
	}
	$bool2 = true;
	if($tipo==0 || $tipo==3) {
		$id = $wpdb->get_var("SELECT idUsuario FROM {$wpdb->prefix}weblaborpress_asignada WHERE idEvento = $idEvento && tipoPersona=2");
		$html .= "<br /><b>Fotógrafo:</b> ";
		if(isset($id)) {
			$user = get_userdata($id);
			$html .="<span class='name'>";
			$html .= $user->display_name;
			$html .="<div class='info'><b>Fotos: </b>";
			$html .= countFotos($id);
			$html .="</div>";
			$html .="</span>";
			if(countFotos($id) > countFotos($current_user->ID) && $id != $current_user->ID) {
				$bool2 = false;
			}
			if($bool2 && $evento->fecha!="0000-00-00 00:00:00") {
				$fechaActual = new DateTime(date("Y-m-d H:m")); 
				$fecha = new DateTime($evento->fecha);
				$interval = date_diff($fechaActual, $fecha);
				if($interval->invert==0 && $interval->d<=5) {
					$bool2 = false;
				}
			}	
		}
		if($bool && $bool2) {
			$html .= " (<a href='admin.php?page=weblaborpress&agregar=$idEvento&tipo=2'>";
			if($id != $current_user->ID && $b2) {
				$html .= "Asignarme";
			} else if($id == $current_user->ID) {
				$html .= "Desasignarme";
			}
			$html .= "</a>)";		
		}
		
	}

	return $html;
}
function countEvents($idUsuario){
	global $wpdb;
	return $wpdb->get_var("SELECT count(*) FROM {$wpdb->prefix}weblaborpress_eventos WHERE creador = $idUsuario");
}
function countNotas($idUsuario){
	global $wpdb;
	return $wpdb->get_var("SELECT count(*) FROM {$wpdb->prefix}weblaborpress_eventos natural join {$wpdb->prefix}weblaborpress_asignada WHERE idUsuario = $idUsuario and hecho = 1 and tipoPersona=1");
}
function countFotos($idUsuario){
	global $wpdb;
	return $wpdb->get_var("SELECT count(*) FROM {$wpdb->prefix}weblaborpress_eventos natural join {$wpdb->prefix}weblaborpress_asignada WHERE idUsuario = $idUsuario and hecho = 1 and tipoPersona=2");
}
function fechaNoActiva($fecha) {
	if($fecha!="0000-00-00 00:00:00") {
		$fechaActual = new DateTime(date("Y-m-d H:m")); 
		$fecha = new DateTime($fecha);
		if($fechaActual > $fecha) {
			return true;
		}
	}
	
	return false;
}
function estadoPago($estado) {
	if($estado==1) {
		return "Pagado";
	}
	return "Pendiente";
}
?>